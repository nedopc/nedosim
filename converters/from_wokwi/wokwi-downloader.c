/* wokwi-downloader.c - unofficial downloader of Wokwi projects */

/* This code was created by Shaos in June 2023 and moved to PUBLIC DOMAIN */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define WOKWI "https://wokwi.com/projects/"

#define HTML "index.html"

char wokwi_id[32]="";
char wokwi_file[32]="";

int main(int argc, char** argv)
{
 char str[256],st2[32];
 FILE *f,*fout;
 unsigned char *buf,*ptr,*pt2;
 unsigned long sz;
 int i,j,k,err=0;

 if(argc<2)
 {
    printf("\nwokwi-downloader wokwi-id\n\n");
    return -1;
 }
 strcpy(wokwi_id,argv[1]);
 if(argc>2)
 {
    strcpy(wokwi_file,argv[2]);
 }
 unlink(HTML);
 sprintf(str,"wget -O %s %s%s",HTML,WOKWI,wokwi_id);
 printf("\n%s\n\n",str);
 system(str);
 f = fopen(HTML,"rb");
 if(f==NULL)
 {
    printf("\nERROR: Can't download Wokwi project!\n\n");
    return -2;
 }
 fseek(f,0,SEEK_END);
 sz = ftell(f);
 fseek(f,0,SEEK_SET);
 buf = (unsigned char*)malloc(sz+1);
 if(buf==NULL)
 {
    printf("\nERROR: Out of memory!\n\n");
    return -3;
 }
 fread(buf,sz,1,f);
 fclose(f);
 buf[sz] = 0;
 ptr = buf;
 i = 0;
 while(*ptr)
 {
   if(!strncmp(ptr,"\"name\"",6))
   {
      ptr += 6;
      if(*ptr!=':'){err=-101;break;}
      ptr++;
      if(*ptr!='"'){err=-102;break;}
      ptr++;
      pt2 = strchr(ptr,'"');
      if(pt2==NULL){err=-103;break;}
      *pt2 = 0;
      strncpy(str,ptr,255);
      str[255] = 0;
      printf("[%i] %s\n",i++,str);
      ptr = pt2+1;
      if(!strncmp(ptr,",\"id\"",5))
      {
         ptr += 5;
         if(*ptr!=':'){err=-104;break;}
         ptr++;
         if(*ptr!='"'){err=-105;break;}
         ptr++;
         pt2 = strchr(ptr,'"');
         if(pt2==NULL){err=-106;break;}
         *pt2 = 0;
         strncpy(st2,ptr,32);
         st2[31] = 0;
         ptr = pt2;
         if(strcmp(st2,wokwi_id)){err=-107;break;}
         strcat(str,".txt");
         for(j=0;j<strlen(str);j++) if(str[j]==' ') str[j]='_';
         fout = fopen(str,"wt");
         if(fout==NULL){err=-108;break;}
         fprintf(fout,"Downloaded from %s%s\n",WOKWI,wokwi_id);
         fprintf(fout,"\nby wokwi-downloader\n");
         fclose(fout);
      }
      else if(!strncmp(ptr,",\"content\"",10))
      {
         ptr += 10;
         if(*ptr!=':'){err=-109;break;}
         ptr++;
         if(*ptr!='"'){err=-110;break;}
         ptr++;
         for(j=0;j<strlen(str);j++) if(str[j]==' ') str[j]='_';
         fout = fopen(str,"wt");
         if(fout==NULL){err=-111;break;}
         while(*ptr && *ptr!='"')
         {
            if(*ptr=='\\')
            {
               ptr++;
               if(*ptr=='r') *ptr = 0x0D;
               if(*ptr=='n') *ptr = 0x0A;
               if(*ptr=='t') *ptr = 0x09;
               if(*ptr=='u')
               {
                  st2[0] = ptr[1];
                  st2[1] = ptr[2];
                  st2[2] = ptr[3];
                  st2[3] = ptr[4];
                  st2[4] = 0;
                  ptr += 4;
                  k = (unsigned char)strtol(st2, NULL, 16);
                  if(k<0 || k>255){err=-112;break;}
                  *ptr = (unsigned char)k;
               }
            }
            fputc(*ptr,fout);
            ptr++;
         }
         if(err<0) break;
         fclose(fout);
      }
      else{err=-113;break;}
   }
   ptr++;
 }
 printf("\n%i names retrieved\n",i);
 if(err)
 {
   printf("\nERROR: Something is wrong (%i)\n\n",err);
 }
 free(buf);
 return err;
}
