`default_nettype none

module wokwi(
  input I1,
  input I2,
  output OUT
);

  wire net17;
  wire net25;
  wire net26;
  wire net27;
  wire net28;
  wire net29;
  wire net30;

  assign OUT = net17;

  and_cell gate8 (
    .a (net25),
    .b (net26),
    .out (net27)
  );
  or_cell gate9 (
    .a (net25),
    .b (net26),
    .out (net28)
  );
  and_cell gate10 (
    .a (net28),
    .b (net29),
    .out (net30)
  );
  or_cell gate11 (
    .a (net27),
    .b (net30),
    .out (net29)
  );
  buffer_cell gate12 (
    .in (I2),
    .out (net26)
  );
  buffer_cell gate13 (
    .in (I1),
    .out (net25)
  );
  buffer_cell gate14 (
    .in (net29),
    .out (net17)
  );

endmodule

module buffer_cell (
    input in,
    output out
    );
    assign out = in;
endmodule

module and_cell (
    input a,
    input b,
    output out
    );
    assign out = a & b;
endmodule

module or_cell (
    input a,
    input b,
    output out
    );
    assign out = a | b;
endmodule
