// Wokwi Custom Chip - For information and examples see:
// https://link.wokwi.com/custom-chips-alpha
//
// SPDX-License-Identifier: MIT
// Copyright (C) 2022 Uri Shaked / wokwi.com
// Made configurable by Shaos in December 2022

#include "wokwi-api.h"
#include <stdio.h>
#include <stdlib.h>

#define MAXIO 16

typedef struct {
  pin_t ins[MAXIO];
  pin_t outs[MAXIO];
} chip_state_t;

void update_pins(chip_state_t *chip) {
  for (int i = 0; i < MAXIO; i++) {
    pin_write(chip->outs[i], pin_read(chip->ins[i]));
  }
}

static void chip_pin_change(void *user_data, pin_t pin, uint32_t value) {
  update_pins((chip_state_t*)user_data);
}

void chip_init() {
  chip_state_t *chip = malloc(sizeof(chip_state_t));

  char name[8];
  for(int i1=0;i1<MAXIO;i1++)
  {
      sprintf(name,"EXTIN%d",i1);
      chip->ins[i1] = pin_init(name, INPUT_PULLDOWN);
  }
  for(int i2=0;i2<MAXIO;i2++)
  {
      sprintf(name,"IN%d",i2);
      chip->outs[i2] = pin_init(name, OUTPUT);
  }

  update_pins(chip);

  pin_watch_config_t watch_config = {
    .edge = BOTH,
    .pin_change = chip_pin_change,
    .user_data = chip,
  };
  for (int i = 0; i < MAXIO; i++) {
    pin_watch(chip->ins[i], &watch_config);
  }
  
  // The following message will appear in the browser's DevTools console:
  printf("Hello from input %i pins!\n",MAXIO);
}

