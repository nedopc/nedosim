// Wokwi Custom Chip - For information and examples see:
// https://link.wokwi.com/custom-chips-alpha
//
// SPDX-License-Identifier: MIT
// Copyright (C) 2022 Uri Shaked / wokwi.com
// Modified by Shaos in June 2023

#include "wokwi-api.h"
#include <stdio.h>
#include <stdlib.h>

#define MAXIO 2

typedef struct {
  pin_t ins[MAXIO];
  pin_t outs[MAXIO];
} chip_state_t;

void update_pins(chip_state_t *chip) {
  for (int i = 0; i < MAXIO; i++) {
    pin_write(chip->outs[i], pin_read(chip->ins[i]));
  }
}

static void chip_pin_change(void *user_data, pin_t pin, uint32_t value) {
  update_pins((chip_state_t*)user_data);
}

void chip_init() {
  chip_state_t *chip = malloc(sizeof(chip_state_t));

  char name[8];
  sprintf(name,"EXTCLK");
  chip->ins[0] = pin_init(name, INPUT_PULLDOWN);
  sprintf(name,"EXTNRST");
  chip->ins[1] = pin_init(name, INPUT_PULLDOWN);
  sprintf(name,"CLK");
  chip->outs[0] = pin_init(name, OUTPUT);
  sprintf(name,"NRST");
  chip->outs[1] = pin_init(name, OUTPUT);

  update_pins(chip);

  pin_watch_config_t watch_config = {
    .edge = BOTH,
    .pin_change = chip_pin_change,
    .user_data = chip,
  };
  pin_watch(chip->ins[0], &watch_config);
  pin_watch(chip->ins[1], &watch_config);
  
  // The following message will appear in the browser's DevTools console:
  printf("Hello from input %i pins!\n",MAXIO);
}

